<?php
$tag = 'new';
$page = 1;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	scraping();
}
function scraping()
{
	require_once 'class.HttpRequest.php';
	$http = new Httprequest();
	if (isset($_POST['page'])) {
		$page = $_POST['page'];
	}
	if (isset($_POST['tag'])) {
		$tag = $_POST['tag'];
	}
	if (isset($_POST['next'])) {
		$page = $page + 1;
	}
	if (isset($_POST['access_token'])) {
		$access_token = $_POST['access_token'];
	}
	if (isset($_POST['domain'])) {
		$http->setServer($_POST['domain'].$tag.'/'.$page);
	}
	$html = $http->send();
	if (isset($html->error)) {
		echo json_encode($html); exit();
	}
	$result = $html->contents;
	if (isset($_POST['tag-element'])) {
		$response = array();
		$checkData = $result->find($_POST['tag-element']);
		if (count($checkData)) {
			foreach ($checkData as $key => $check) {
				array_push($response, $check->outertext());
			}
		}

		echo json_encode($response); exit();
	}
}					

?>
<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<link rel="stylesheet" href="css/style.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<div class="modal fade" id="modal-id">
			<div class="modal-dialog">
				<form action="javascrip:void(0);" method="post" accept-charset="utf-8" id="form-post-to-fb">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Post to pages</h4>
						</div>
						<div class="modal-body">
							<input type="hidden" id="id" name="id" value="">
							<div class="form-group">
								<label class="" for="">Title</label>
								<input type="text" readonly="readonly" class="form-control" id="message" placeholder="Input field">
							</div>
							<div class="form-group">
								<label class="" for="">Picture</label>
								<a href="#" class="thumbnail">
									<img data-src="#" id="picture" name="img" class="img-responsive" alt="Image" style="margin: 0 auto; width: 50%;">
								</a>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Post</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div id="fb-root"></div>
		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.9&appId=1771663706422284";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
		</script>
		<div class="container-fluid">
			<div class="page-header">
			  <h1>Scraping html tag elements<small> v1.1</small> <div class="fb-login-button pull-right" data-max-rows="1" data-size="medium" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="true" data-use-continue-as="false"></div></h1>
			</div>
			<div class="container-fluid">
				<div class="col-xs-7 col-sm-7 col-md-7 col-lg-7">
					<div class="panel panel-info" style="position: fixed; width: 50%;">
						<div class="panel-heading">
							<h3 class="panel-title">Scraping options</h3>
						</div>
						 <div id="fb-root"></div>
				        <script>
				        ACCESS_TOKEN = '';
						window.fbAsyncInit = function() {
						    FB.init({
						        appId: '1771663706422284',
						        autoLogAppEvents: true,
						        status: true,
						        xfbml: true,
						        version: 'v2.9' // or v2.8, v2.7, v2.6, v2.5, v2.4, v2.3,
						    });
						    function login() {
							    FB.getLoginStatus(function(response) {
							        if (response.status === 'connected') {
							            UID = response.authResponse.userID;
							            access_token = (response.authResponse.accessToken);
							            FB.api('/1084324198370688?fields=access_token', 'get', {
							            	access_token: ACCESS_TOKEN
							            }, function(response) {
							            	console.log(response);
								            console.log(response.access_token);
									    	ACCESS_TOKEN = response.access_token;
									    }, {scope : 'puclic_pages'});
							        } else if (response.status === 'not_authorized') {
							            // the user is logged in to Facebook, 
							            // but has not authenticated your app
							        } else {
							            FB.login(function(response) {
							            	UID = response.authResponse.userID;
							            	access_token = (response.authResponse.accessToken);
							                FB.api('/1084324198370688?fields=access_token', 'get', {
								            	access_token: ACCESS_TOKEN
								            }, function(response) {
								            	console.log(response);
								            	console.log(response.access_token);
										    	ACCESS_TOKEN = response.access_token;
										    }, {scope : 'puclic_pages'});
							            });
							        }
							    });
							}
							login();
						};
				        </script>
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
									<form action="" method="POST" class="form" role="form" id="scraping-form">
										<div class="form-group">
											<label class="" for="">URL scraping: <span> http://example.com</span></label>
											<input type="text" name="domain" id="inputURL" class="form-control" value="http://xem.vn/" required="required" title="URL Scrap target" placeholder="Enter a domain">
										</div>
										<div class="form-group">
											<label class="" for="">Topic: <span> new, vote</span></label>
											<input type="text" name="tag" id="inputTag" class="form-control" value="<?php echo @$tag; ?>" title="Topic target" placeholder="Enter a topic target">
										</div>
										<div class="form-group">
											<label class="" for="">Page number: <span> 1, 2</span></label>
											<input type="text" name="page" id="inputPage" class="form-control" value="<?php echo @$page; ?>" title="Page number" placeholder="Enter page number">
										</div>
										<div class="form-group">
											<label class="" for="">Tag finds: <span>(#elements, .classes)</span></label>
											<input type="text" name="tag-element" id="inputTagElement" class="form-control" value="div.photoListItem" required="required" title="Target finds" placeholder="Enter a html tag elements">
										</div>
										<div class="form-group">
											<label class="" for="">Display type</label>
											<div class="radio">
												<label>
													<input type="radio" name="display-html" value="0" checked="checked">
													Html elements
												</label>
											</div>
											<div class="radio">
												<label>
													<input type="radio" name="display-html" value="1">
													Html string
												</label>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-sm-12">
													<div class="input-group">
														<input type="submit" name="next" value="Next" class="btn btn-primary pull-right btn-padding">
														<button type="submit" name="submit" value="submit" class="btn btn-primary pull-right btn-padding">Get  data</button>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">
					<div class="panel panel-info">
						<div class="panel-heading">
							<h3 class="panel-title">Respond Scraping</h3>
						</div>
						<div class="panel-body" style="text-align: center;overflow: scroll;">
							Display result Html here
							<div id="result" style="text-align: center;">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="js/script-public.js"></script>
	</body>
</html>