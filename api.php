<?php
header('Access-Control-Allow-Origin: *');
function scraping($tag = 'new', $page = 0, $tag_element = '.photoListItem .thumbnail a img.thumbImg')
{	
	if (isset($_POST['tag'])) {
		$tag = $_POST['tag'];
	}
	if (isset($_POST['page'])) {
		$page = $_POST['page'];
	}
	if (isset($_POST['tag_element'])) {
		$tag_element = $_POST['tag_element'];
	}
	require_once 'class.HttpRequest.php';
	$http = new Httprequest();
	$http->setServer('http://xem.vn/'.$tag.'/'.$page);
	$html = $http->send();
	if (isset($html->error)) {
		echo json_encode($html); exit();
	}
	$result = $html->contents;
	if ($result) {
		$response = array();
		$checkData = $result->find($tag_element);
		if (count($checkData)) {
			foreach ($checkData as $key => $check) {
				$title = @$check->parent->attr['title'];
				$check->attr['title'] = $title ? $title : $check->attr['alt'];
				array_push($response, $check->attr);
			}
		}
		echo json_encode($response);
	}
	else {
		echo json_encode(array());
	}
}					
scraping();
?>