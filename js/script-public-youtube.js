$(document).ready(function() {
	$('#result').html("<img class='gif-default' src='img/default.gif' />");
	setTimeout(function () {
		$('#scraping-form').trigger('submit');
	}, 3000);
	$(document).on('click', '.post-to-fb', function () {
		var id = $(this).data('id');
		if (typeof DATA[id] != "undefined") {
			$('#modal-id').find("#id").val(DATA[id].id);
			$('#modal-id').find("#message").val(DATA[id].message);
			$('#modal-id').find("#picture").attr('src', DATA[id].picture);
			$('#modal-id').modal('show');
		};
	});
	DATA = [];
	UID = '';
	$(document).on('click', '#form-post-to-fb button[type="submit"]', function (event) {
		var id = $(this).parents('form#form-post-to-fb').find('#id').val();
		console.log(id);
		post_with_token(id);
	});
    $('#scraping-form').submit(function(event) {
    	var $btn = $(document.activeElement);
        event.preventDefault();
        var data = $(this).serialize();
        if ($btn.val() == "Next") {
        	data += '&' + $btn.attr('name') +'=' + $btn.val();
        	number = $("#inputPage").val();
        	$("#inputPage").val(parseInt(number) + 1);
        }
        $.ajax({
            type: "POST",
            url: 'index-youtube-xemvn.php',
            data: data,
            beforeSend: function() {
                $('#result').html("<img class='gif-default' src='img/default.gif' />");
            },
            complete: function() {
                $('#result img.gif-default').remove();
            },
            success: function(result) {
                if (result.length > 0) {
                    result = JSON.parse(result);
                    console.log(Object.keys(result).length);
                    if (Object.keys(result).length > 0) {
                        if (typeof result.error != "undefined") {
                            alert(result.message);
                        };
                        $("#result").html('');
                        var list = [];
                        for (var i = 0; i < result.length; i++) {
                            var encode = $('<div />').text(result[i]).html();
                            var encodedStr = $("<div />").html(encode).text();
                            var appendText = '';
                            if ($('input[name="display-html"]:checked', 'form').val() != 1) {
                                appendText = encodedStr;
	                            var eleVideo = $('<div></div>').html(result[i]);
	                            console.log(result[i]);
	                            var item = {};
	                            item.src = eleVideo.find('img')[0].src;
					            item.alt = eleVideo.find('img')[0].alt;
					            item.title = eleVideo.find('.yt-lockup-content a')[0].title;
					            item.href = $("#inputURL").val() + eleVideo.find('.yt-lockup-content a')[0].href.replace(window.location.origin + '/', '');
					            item.id = eleVideo.find('button.yt-uix-button.yt-uix-button-size-small.yt-uix-button-default.yt-uix-button-empty.yt-uix-button-has-icon.no-icon-markup.addto-button.addto-queue-button.video-actions.spf-nolink.hide-until-delayloaded.addto-tv-queue-button.yt-uix-tooltip').data('video-ids');
                            	DATA[item.id] = item;
                            	console.log(item.href);
	                            var entries = 	'<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> \
									<a href="' + item.href + '" target="_blank" class="thumbnail">' + item.id + '---' + item.title + ' \
										<img src="' + item.src + '" alt=""> \
									</a> \
								</div>';
                                $("#result").append(entries);
                            } else {
                                appendText = encode;
                                $("#result").append('<code>' + appendText + '</code>');
                            }
                        };
                    } else {
                        alert('The tag element: ' + $("#inputTagElement").val() + ' is not found');
                    }
                } else {
                    alert('The tag element: ' + $("#inputTagElement").val() + ' is not found');
                }
            }
        });
    });
	
	// function login() {
	//     FB.getLoginStatus(function(response) {
	//     	console.log(response);
	//         if (response.status === 'connected') {
	//             UID = response.authResponse.userID;
	//             ACCESS_TOKEN = (response.authResponse.accessToken);
	//             console.log(response.authResponse.accessToken);
	//             FB.api('/1084324198370688?fields=access_token', 'get', {
	//             	access_token: ACCESS_TOKEN
	//             }, function(response) {
	// 		    	console.log(response);
	// 		    });
	//         } else if (response.status === 'not_authorized') {
	//             // the user is logged in to Facebook, 
	//             // but has not authenticated your app
	//         } else {
	//             FB.login(function(response) {
	//             	UID = response.authResponse.userID;
	//             	ACCESS_TOKEN = (response.authResponse.accessToken);
	//                 FB.api('/1084324198370688?fields=access_token', 'get', {
	// 	            	access_token: ACCESS_TOKEN
	// 	            }, function(response) {
	// 			    	console.log(response);
	// 			    });
	//             });
	//         }
	//     });
	// }

	// function logout() {
	//     FB.logout(function(response) {
	//         // user is now logged out
	//     });
	// }

	function post_with_token(id) {
		if (typeof DATA[id] == "undefined") {
			alert("Data not found");
		};
	    FB.api('/1084324198370688/photos?access_token='+ACCESS_TOKEN, 'post', {
	            message: DATA[id].message,
	            picture: DATA[id].picture,
	            url: DATA[id].picture,
	            name: DATA[id].name,
	            from: 1084324198370688,
	            description: ''
	        },
	        function(response) {
	        	$('#modal-id').modal('hide');
	            console.log(response);
	            if (typeof response.error != "undefined") {
	            	alert(response.error.message);
	            	return false;
	            };
	            var id = response.id;
	            // FB.ui({
	            //     method: 'share',
	            //     href: 'https://www.facebook.com/photo.php?fbid='+id+'&set=o.'+1084324198370688+'&type=3&theater',
	            // }, function(response) {
	            // 	console.log(response);
	            // });
	        });
	}
});
