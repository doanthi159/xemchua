$(document).ready(function() {
	$('#result').html("<img class='gif-default' src='img/default.gif' />");
	setTimeout(function () {
		$('#scraping-form').trigger('submit');
	}, 3000);
	$(document).on('click', '.post-to-fb', function () {
		var id = $(this).data('id');
		if (typeof DATA[id] != "undefined") {
			$('#modal-id').find("#id").val(DATA[id].id);
			$('#modal-id').find("#message").val(DATA[id].message);
			$('#modal-id').find("#picture").attr('src', DATA[id].picture);
			$('#modal-id').modal('show');
		};
	});
	DATA = [];
	UID = '';
	$(document).on('click', '#form-post-to-fb button[type="submit"]', function (event) {
		var id = $(this).parents('form#form-post-to-fb').find('#id').val();
		console.log(id);
		post_with_token(id);
	});
    $('#scraping-form').submit(function(event) {
    	var $btn = $(document.activeElement);
        event.preventDefault();
        var data = $(this).serialize();
        if ($btn.val() == "Next") {
        	data += '&' + $btn.attr('name') +'=' + $btn.val();
        	number = $("#inputPage").val();
        	$("#inputPage").val(parseInt(number) + 1);
        }
        $.ajax({
            type: "POST",
            url: 'index-public.php',
            data: data,
            beforeSend: function() {
                $('#result').html("<img class='gif-default' src='img/default.gif' />");
            },
            complete: function() {
                $('#result img.gif-default').remove();
            },
            success: function(result) {
                if (result.length > 0) {
                    result = JSON.parse(result);
                    if (Object.keys(result).length > 0) {
                        if (typeof result.error != "undefined") {
                            alert(result.message);
                        };
                        $("#result").html('');
                        for (var i = 0; i < result.length; i++) {
                            var encode = $('<div />').text(result[i]).html();
                            var encodedStr = $("<div />").html(encode).text();
                            var appendText = '';
                            if ($('input[name="display-html"]:checked', 'form').val() != 1) {
                                appendText = encodedStr;
                                var element = $(appendText);
	                            if (typeof element.data('id') == "undefined") {
	                            	continue;
	                            };
	                            var thumb = element.find('div.thumbnail img').attr('src');
	                            var title = element.find('div.info a').attr('title');
	                            var item = {
	                            	id: element.data('id'),
	                            	message: title,
	                            	picture: thumb,
	                            	name: title
                            	};
                            	DATA[element.data('id')] = item
	                            var entries = 	'<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" data-id="'+element.data('id')+'"> \
		            					            <h4 class="text-left title">'+title+'<span class="btn btn-default btn-small pull-right post-to-fb" data-id="'+element.data('id')+'">Post</spoan></h4> \
		            					            <img src="'+thumb+'" class="img-responsive img-thumb"> \
		            					        </div>';
                                $("#result").append(entries);
                            } else {
                                appendText = encode;
                                $("#result").append('<code>' + appendText + '</code>');
                            }
                        };
                    } else {
                        alert('The tag element: ' + $("#inputTagElement").val() + ' is not found');
                    }
                } else {
                    alert('The tag element: ' + $("#inputTagElement").val() + ' is not found');
                }
            }
        });
    });
	
	// function login() {
	//     FB.getLoginStatus(function(response) {
	//     	console.log(response);
	//         if (response.status === 'connected') {
	//             UID = response.authResponse.userID;
	//             ACCESS_TOKEN = (response.authResponse.accessToken);
	//             console.log(response.authResponse.accessToken);
	//             FB.api('/1084324198370688?fields=access_token', 'get', {
	//             	access_token: ACCESS_TOKEN
	//             }, function(response) {
	// 		    	console.log(response);
	// 		    });
	//         } else if (response.status === 'not_authorized') {
	//             // the user is logged in to Facebook, 
	//             // but has not authenticated your app
	//         } else {
	//             FB.login(function(response) {
	//             	UID = response.authResponse.userID;
	//             	ACCESS_TOKEN = (response.authResponse.accessToken);
	//                 FB.api('/1084324198370688?fields=access_token', 'get', {
	// 	            	access_token: ACCESS_TOKEN
	// 	            }, function(response) {
	// 			    	console.log(response);
	// 			    });
	//             });
	//         }
	//     });
	// }

	// function logout() {
	//     FB.logout(function(response) {
	//         // user is now logged out
	//     });
	// }

	function post_with_token(id) {
		if (typeof DATA[id] == "undefined") {
			alert("Data not found");
		};
	    FB.api('/1084324198370688/photos?access_token='+ACCESS_TOKEN, 'post', {
	            message: DATA[id].message,
	            picture: DATA[id].picture,
	            url: DATA[id].picture,
	            name: DATA[id].name,
	            from: 1084324198370688,
	            description: ''
	        },
	        function(response) {
	        	$('#modal-id').modal('hide');
	            console.log(response);
	            if (typeof response.error != "undefined") {
	            	alert(response.error.message);
	            	return false;
	            };
	            var id = response.id;
	            // FB.ui({
	            //     method: 'share',
	            //     href: 'https://www.facebook.com/photo.php?fbid='+id+'&set=o.'+1084324198370688+'&type=3&theater',
	            // }, function(response) {
	            // 	console.log(response);
	            // });
	        });
	}
});
